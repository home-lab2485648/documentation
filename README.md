![Diátaxis](Diátaxis.png)
This repo is organized according to the Diátaxis framework.
In short, there are 4 different things which we call documentation: tutorials, how-to guides, reference material, and explanation. Understanding which you are writing as you write it, and organizing documents according to their type helps to create more usable documentation.
Tutorials are a lesson on how to do something. How-to guides are a high level guide on how to do something (like a recipe in cooking). Reference material is what it sounds like, and explanation intends to provide higher level insight (high level concepts, best practice, etc.).
You can get a more in depth explanation of the framework at https://diataxis.fr/.