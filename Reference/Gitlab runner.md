I have a Gitlab runner installed on my mac, so that I can do CI/CD stuff with my home lab.

I would like to just have it running on one of the servers, but having configuration changes happen on the same server as the runner that is making the changes seems like a good way to bork things without a great way to recover it. 

I also considered installing it on a Raspberry Pi or something, but just doing it on my Mac seems easier.

The installation process is documented at [Set up Gitlab runner on MacOS](documentation/Tutorials/Set%20up%20Gitlab%20runner%20on%20MacOS.md)
