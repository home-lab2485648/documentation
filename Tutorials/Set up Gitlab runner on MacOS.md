This tutorial will walk through setting up a Gitlab runner on MacOS, using Docker as the executor. 
This tutorial assumes that you already have docker and Homebrew installed on your machine. It also assumes that Docker has already been authenticated to registry.gitlab.com using the `docker login` command. 
[Instructions on installing Docker can be found here](https://docs.docker.com/desktop/install/mac-install/).
My original plan was to install it as a docker service I couldn't get it working. I would guess that it's because using docker as the executor in a docker service doesn't work. 
I also wanted to use Homebrew to install it, but it seems like using Homebrew to install it, it tries pulling a gitlab-runner-helper container with a tag that does not exist.
## Install the runner binary
First download the binary for your architecture.
For Intel Macs, run:
```zsh
sudo curl --output /usr/local/bin/gitlab-runner "https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-darwin-amd64"
```
For Apple Silicon Macs, run:
```zsh
sudo curl --output /usr/local/bin/gitlab-runner "https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-darwin-arm64"
```
Then adjust the permissions so that it is executable by running this command:
```zsh
sudo chmod 755 /usr/local/bin/gitlab-runner
```
## Obtain runner auth token
I'm installing the runner as a group runner, meaning that any project in the group will have access to it.
1. From the group home page, use the navigation bar on the left to get to Build > Runners. There, select "New group runner"
2. Set the platform to MacOS
3. Select the option to "Run untagged jobs"
4. Select "Create Runner"
5. Either note the URL and token from the command it gives you, or just keep the window open for the next step.
## Register runner
Run the following command, replacing $URL and $TOKEN with the values from the last step.
```zsh
gitlab-runner register \
  --non-interactive \
  --url "$URL" \
  --token "$TOKEN" \
  --executor "docker" \
  --docker-image alpine:latest \
  --description "docker-runner"

```
You can set a different description, or a different docker image if you want. 
## Install runner as a service
Run the following command.
```zsh
gitlab-runner install && gitlab-runner start
```
One note on this step is that it needs to be run as the user that will be logged in. It is set to start at login, not at boot. Something about launchctl makes running as a system service not work. 
## Docker service
If you don't already have Docker Desktop running at start up, get it running at startup so that the runner can access the docker engine. 
## Apple Silicon Mac
If you are doing this on an Apple Silicon mac, you will need to modify `~/.gitlab-runner/config.toml`. Add this line under \[runners.docker\], replacing \[user\] with your username. If you have multiple runners registered, make sure to get it under the right one.
```
host = "unix://Users/[user]/.docker/run/docker.sock"
```
## References
Most of this guide is based off of this page. https://docs.gitlab.com/runner/install/osx.html
The Apple Silicon Mac section is based off of this page. https://gitlab.com/gitlab-org/gitlab-runner/-/issues/29378