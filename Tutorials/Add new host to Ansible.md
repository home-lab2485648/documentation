This tutorial will explain how to run Ansible against a new host.

We will add the host to the inventory file, run the bootstrap_ansible_user playbook from your local machine to add the ansible user to the new host, then we will commit and push our change so that CI/CD can handle applying any roles to the new host. 

This tutorial assumes that you have an existing user with admin access on the host, that the user needs to use a password to run sudo commands on the host, and that the user is accessible via ssh. It is also assumed that your working directory is set to the root of the ansible repo. 

## Add host to inventory file

The first thing to do is to add the host to the inventory file. You can just add it as a host, or you can add it to whatever groups you want. It just needs to be in there somewhere for this playbook to work.

I would not commit this change to Git yet. We will commit and push the change after we've run the bootstrap playbook, so that pipelines can handle the rest of the configuration as the ansible user. 

## Define public key variable

Set $ANSIBLE_PUBLIC_KEY equal to the public ssh key. Right now this value is stored as a CI/CD variable for the home-lab/ansible project in Gitlab.

```bash
export ANSIBLE_USER_PUBLIC_KEY='<insert public key here>'
```

## Run the playbook

Run the "bootstrap_ansible_user.yaml" playbook. You will be prompted to enter the host(s) for the playbook to be run against. Usually this will just be the actual hostname as it is listed in the inventory that you are using.

```bash
 ansible-playbook -i <inventory file> bootstrap_ansible_user.yaml --ask-become-pass
```

## Commit and push inventory change

Now that the ansible user and it's public key have been put onto the new host, we can push the updated inventory file. Once the change has been pushed, CI/CD should handle applying any roles assigned to the host or its group. 