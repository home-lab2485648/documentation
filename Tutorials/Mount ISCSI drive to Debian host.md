This Tutorial assumes that open-iscsi is already installed.

## Setting up CHAP
If you're using CHAP, you need to add a few entries to `/etc/iscsi/iscsi.conf` (Ignore this step if you aren't using CHAP). These lines are probably already there, but commented out, so find them, uncomment them, then fill in the values.
``` key-value
node.session.auth.authmethod = CHAP
node.session.auth.username = <your CHAP username>
node.session.auth.password = <your CHAP password>
```

If you are using CHAP for discovery (As of writing this, Synology DSM 7 doesn't support this) then you need to do the same for these lines. These values may or may not be the same as those above.
```
discovery.sendtargets.auth.authmethod = CHAP
discovery.sendtargets.auth.username = <your CHAP username>
discovery.sendtargets.auth.password = <your CHAP password>
```

## Discovering and logging into ISCSI targets
To discover what targets are available on a host, run this command. 
``` bash
iscsiadm  --mode discovery --type sendtargets --portal <server ip address>
```
This should return a list of ISCSI targets.

To connect to a target, run this command. The target name will be in this format `iqn.2007-01.org.debian.foobar:USB`.
``` bash
iscsiadm  --mode node  --targetname "<target name>" --portal "<server ip address>:3260" --login
```
This will return some info on whether it was successful or not. 

At this point, if things are working correctly, another block device should have appeared under `/dev`. It should also show up under `/dev/disk/by-path/ip-<server ip address>:3260p-iscsi-<target name>-lun-<lun number>`. From there, you mount format, and use the drive like you would a physical one.

If the ISCSI drive doesn't show up under /dev, logout using the below command, re "discover" the target using the first command, then try logging in again. 

To disconnect from the target, run this command.
``` bash
iscsiadm  --mode node  --targetname "<target name>" --portal "<server ip address>:3260" --logout
```

*Note: For most ISCSI stuff, using hostnames is iffy. It might work, it might fail. Just use IP addresses.*