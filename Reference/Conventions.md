These conventions aren't perfectly followed, because I'm learning as I'm going. Making old stuff match convention is a work in progress.
## Names
#### File Names
All file names and variables should use dashes to separate words in file and variable names. Unless it is part of another convention (like constant variable names), everything should be lowercase (this includes. For example, `file-name.sh`, or `variable-name.sh`. 
numbers after words do not need a leading underscore. For example, `python3`. Do not do `python-3`.
Names should avoid pointless abbreviation. It just creates ambiguity in most cases. 
Use `.yaml` instead of `.yml` when possible.
#### Host names
Host names are all lowercase, with dashes separating words. Numbers after words do not need a leading dash. For example, `debian12.lan` or `joshua-latitude.lan` are both good. 
#### Environment Names
Environment names are one of places where abbreviation is used. They are unique and recognizable enough that I'm not worried about confusion, and they are frequently used when naming stuff in AWS, which often has limits on length. 
Right now I've got "test" and "prod". I want 5 letters to be the maximum length for an environment name so that they can be used consistently in length constrained contexts, and because there isn't a great 3 or 4 letter abbreviation for "stage" which I might use at some point.
#### Service Users
Service usernames should start with the word app (ie. app-terraform).