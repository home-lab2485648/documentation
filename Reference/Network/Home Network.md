## WAN Configuration
- Physical Port: 9
- IP Address: DHCP (jjj.jferry.net is set up with ddns to point at it)
- Subnet Mask: DHCP
- Gateway: DHCP
- DNS Servers: DHCP
---
## SSIDs and passwords
- getmepicturesofspiderman
- jjj_iot
---
## Networks
Routes are generated between all of these networks automatically, but there is a firewall rule blocking all interVLAN traffic, except stuff initiated from Default.
### Default
Address Space: 192.168.10.0/24
DHCP Range: 192.168.10.100 - 192.168.10.254
Other DHCP Stuff:
- Default Gateway - 192.168.10.1
- DNS Server - 192.168.10.1 - This server resolves local addresses. It will usually respect the hostname that a client gives it. For anything else, it just acts as a cache for for whatever DNS the UDM is using
- Lease Time - 7200 seconds
- Domain Name - lan
	- This is mostly useful if you want to force DNS name resolution instead of Windows name resolution, or IPv6 peer name resolution. For example, The hostname joshua-pi, could be resolved using any of the 3 systems. The hostname joshua-pi.lan will force it to resolve the name using DNS, which consequently forces it to use IPv4, if that’s something you care about. When setting a hostname, you don’t have to include the “.lan”, unless the client is ignoring DHCP option 81.
Static Addresses:
- 192.168.10.1 - UDM/Router
- 192.168.10.2 - Honeypot
getmepicturesofspiderman, and most wired clients are on this network.
### Ferry_IoT
Address Space: 192.168.30.0/24
DHCP Range: 192.168.30.6 - 192.168.30.254
Other DHCP Stuff:
- Default Gateway - 192.168.30.1
- DNS Server - 192.168.30.1
- Lease Time - 86400 seconds
Static Addresses:
- 192.168.30.1 - UDM/Router
jjj_iot is on this network.
### DMZ
Address Space: 192.168.40.0/24
DHCP Range: 192.168.40.6 - 192.168.40.254
Other DHCP Stuff:
- Default Gateway - 192.168.40.1
- DNS Server - 192.168.40.1
- Lease Time - 86400 seconds
Static Addresses:
- 192.168.40.1 - UDM/Router
Stuff that is accessible from the internet should go in here. The idea is that this subnet is treated the same way as the internet, so that in the event a server is compromised, it can’t spread to other subnets. Currently Wan:41179 is forwarded to joshua-legion.lan:25565 for minecraft.
### VPN
Address Space: 192.168.20.0/24
All of the stuff normally handled by DHCP is handled by the Wireguard config file on each client. The default config has 192.168.20.1 as the DNS server and default gateway.
I’m not 100% sure how firewall rules work with this network yet. As far as I can tell it is open to all other networks right now.
All VPN clients are a part of this network.
---
## Physical Layout
Red lines represent DOCSIS
Green lines represent MoCA
Black lines represent Ethernet
![[Townhouse Network Diagram.png]]
---
## MoCA
I was a doofus and didn't set static addresses on the moca boxes. It's almost 1am, so I'm not going to worry about figuring it out right now. 
## Static Addresses
| Hostname            | IP Address    | Description                                                                       |
| ------------------- | ------------- | --------------------------------------------------------------------------------- |
| joshua-nas.lan      | 192.168.10.10 | My Synology NAS                                                                   |
| joshua-latitude.lan | 192.168.40.10 | My home lab production server. It's the latitude on the shelf on the server rack. |
